package model;

import model.contact.Contact;
import model.contact.ContactType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TestPerson {

    @Test
    public void testValidation() {

        assertThrows(IllegalArgumentException.class, () -> new Person("Sergey2022", "Kovalenko"));
        assertThrows(IllegalArgumentException.class, () -> new Person("Alexandr", "Petrov0"));
        assertThrows(IllegalArgumentException.class, () -> new Person("Ivan Ivanov", ""));
        assertThrows(IllegalArgumentException.class, () -> new Person("Ivan.Ivanov", ""));
        assertThrows(IllegalArgumentException.class, () -> new Person("Mary", ""));
        assertThrows(IllegalArgumentException.class, () -> new Person("Яроslav", "Bondar"));

    }

    @Test
    public void testCompareTo() {

        assertEquals(0, new Person("Ivan", "Ivanov").compareTo(new Person("Ivan", "Ivanov")));
        assertEquals(0, new Person("Ivan", "Ivanov").compareTo(new Person("Ivan", "ivanov")));
        assertEquals(0, new Person("ivan", "Ivanov").compareTo(new Person("Ivan", "ivanov")));

        assertEquals(-14, new Person("Anatoliy", "Petrov").compareTo(new Person("Oleg", "Petrov")));

        assertEquals(-15, new Person("Виктор", "Муравьев").compareTo(new Person("Станислав", "муравьев")));
        assertEquals(15, new Person("Станислав", "муравьев").compareTo(new Person("Виктор", "Муравьев")));

        assertEquals(-2, new Person("Агата", "Петрова").compareTo(new Person("Глеб", "Соколов")));
        assertEquals(6, new Person("Родион", "Рожков").compareTo(new Person("Виктория", "Кузьмина")));

        assertEquals(-17, new Person("Sarah", "Barnes").compareTo(new Person("Kimberly", "Snyder")));
        assertEquals(1, new Person("Julia", "Santiago").compareTo(new Person("Stephanie", "Rasmussen")));

        assertEquals(975, new Person("Иван", "Иванов").compareTo(new Person("Ivan", "Ivanov")));
        assertEquals(-975, new Person("Ivan", "Petrenko").compareTo(new Person("Иван", "Петренко")));

    }

    @Test
    public void testToString() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        String regex_uuid = "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}";

        String regex_person =
                regex_uuid + "\n([a-zA-Z]+|[а-яА-Я]+) ([a-zA-Z]+|[а-яА-Я]+)\n(\t((Phone: ([+]?\\d{10,13}))|(E-Mail: ([a-z]|([a-z0-9]+[.]?[a-z0-9]+)+)@[a-z]+[.][a-z]{2,4}))\n)*";

        String expected;

        person = new Person("Петр", "Петров");

        expected = "\nПетр Петров\n";

        assertEquals(expected, person.toString().replaceAll(regex_uuid, ""));

        contacts.add(new Contact(ContactType.PHONE, "0987654321"));

        person.setContacts(contacts);

        expected = "\nПетр Петров\n\tPhone: 0987654321\n";

        assertEquals(expected, person.toString().replaceAll(regex_uuid, ""));

        contacts.add(new Contact(ContactType.EMAIL, "petrov2022@gmail.com"));

        person.setContacts(contacts);

        expected = "\nПетр Петров\n\tPhone: 0987654321\n\tE-Mail: petrov2022@gmail.com\n";

        assertEquals(expected, person.toString().replaceAll(regex_uuid, ""));

        contacts.clear();

        person = new Person("Den", "Иванов");

        expected = "\nDen Иванов\n";

        assertEquals(expected, person.toString().replaceAll(regex_uuid, ""));

        person = new Person("Natalie", "Boyle");

        assertTrue(person.toString().matches(regex_person));

        contacts.add(new Contact(ContactType.PHONE, "+380501234567"));
        contacts.add(new Contact(ContactType.PHONE, "380961234321"));
        contacts.add(new Contact(ContactType.EMAIL, "natboy@ukr.net"));

        person.setContacts(contacts);

        assertTrue(person.toString().matches(regex_person));

        contacts.clear();

        person = new Person("Дмитрий", "Петров");

        contacts.add(new Contact(ContactType.EMAIL, "d.petrov.2022@hotmail.com"));
        contacts.add(new Contact(ContactType.EMAIL, "petrovd@gmail.com"));
        contacts.add(new Contact(ContactType.PHONE, "0960000000"));

        person.setContacts(contacts);

        assertTrue(person.toString().matches(regex_person));

        contacts.clear();

        person = new Person("Влад", "sergienko");

        assertTrue(person.toString().matches(regex_person));

    }

}
