package model.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import model.service.network.mapper.PersonMapper;
import org.junit.Test;
import util.ApplicationProperties;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TestNetworkProxyPhoneBook {

    private static final PhoneBookService phoneBook = new NetworkProxyPhoneBook(
            HttpClient.newBuilder().build(), new PersonMapper(), new ObjectMapper(),
            ApplicationProperties.getProperty("test.network.login"),
            ApplicationProperties.getProperty("test.network.password")
    );

    @Test
    public void testAddAndSort() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        assertTrue(phoneBook.add(person));

        assertTrue(phoneBook.sort().contains(person));

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        assertTrue(phoneBook.add(person));

        assertTrue(phoneBook.sort().contains(person));

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        assertTrue(phoneBook.add(person));

        assertTrue(phoneBook.sort().contains(person));

    }

    @Test
    public void testFilter() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Ivan", "Ivanov");
        contacts.add(new Contact(ContactType.PHONE, "+380681234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));
        person.setContacts(contacts);
        phoneBook.add(person);

        contacts = new ArrayList<>();
        person = new Person("Sergey", "Antonenko");
        contacts.add(new Contact(ContactType.PHONE, "380501112111"));
        person.setContacts(contacts);
        phoneBook.add(person);

        List<Person> filtered = phoneBook.filter(ContactType.PHONE);

        boolean contains = false;

        for (Person p : filtered) {
            if (p.getContacts().stream().anyMatch(contact -> contact.getType().equals(ContactType.EMAIL))) {
                contains = true;
                break;
            }
        }

        assertFalse(contains);

        filtered = phoneBook.filter(ContactType.EMAIL);

        contains = false;

        for (Person p : filtered) {
            if (p.getContacts().stream().anyMatch(contact -> contact.getType().equals(ContactType.PHONE))) {
                contains = true;
                break;
            }
        }

        assertFalse(contains);

    }

    @Test
    public void testSearchByName() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertTrue(phoneBook.searchByName("И").contains(person));
        assertTrue(phoneBook.searchByName("и").contains(person));
        assertTrue(phoneBook.searchByName("ван").contains(person));
        assertFalse(phoneBook.searchByName("Ийклмн").contains(person));

    }

    @Test
    public void testSearchByContact() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertTrue(phoneBook.searchByContact("+380931234567").contains(person));
        assertTrue(phoneBook.searchByContact("+").contains(person));
        assertTrue(phoneBook.searchByContact("iiv").contains(person));

        assertFalse(phoneBook.searchByContact("-").contains(person));
        assertFalse(phoneBook.searchByContact("++").contains(person));

    }

    @Test
    public void testExists() {

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "+380931234567")));
        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "+380681112111")));

        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@ukr.net")));
        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "akim@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "iiv@ukr.net")));

    }

}
