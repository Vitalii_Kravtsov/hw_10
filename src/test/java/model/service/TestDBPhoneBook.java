package model.service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import model.service.db.JdbcTemplate;
import org.junit.Test;
import util.ApplicationProperties;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TestDBPhoneBook {

    @Test
    public void testAdd() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = new DBPhoneBook(
                ApplicationProperties.getProperty("test.db.url"),
                ApplicationProperties.getProperty("db.username"),
                ApplicationProperties.getProperty("db.password")
        );

        Person person;
        List<Contact> contacts = new ArrayList<>();

        assertEquals(0, phoneBook.sort().size());

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(1, phoneBook.sort().size());

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(2, phoneBook.sort().size());

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        phoneBook.add(person);

        assertEquals(3, phoneBook.sort().size());

    }

    @Test
    public void testSort() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        List<Person> sorted = phoneBook.sort();

        assertEquals("Andrew Kim", sorted.get(0).toString().split("\n")[1]);

        assertEquals("Иван Иванов", sorted.get(sorted.size() - 1).toString().split("\n")[1]);

    }

    @Test
    public void testFilter() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        List<Person> filtered = phoneBook.filter(ContactType.PHONE);

        assertEquals(2, filtered.size());

        filtered = phoneBook.filter(ContactType.EMAIL);

        assertEquals(2, filtered.size());

    }

    @Test
    public void testSearchByName() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        assertEquals(1, phoneBook.searchByName("и").size());
        assertEquals(1, phoneBook.searchByName("сергей").size());
        assertEquals(1, phoneBook.searchByName("Andrew").size());
        assertEquals(0, phoneBook.searchByName("Андрей").size());

    }

    @Test
    public void testSearchByContact() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        assertEquals(1, phoneBook.searchByContact("+").size());
        assertEquals(1, phoneBook.searchByContact("380").size());
        assertEquals(0, phoneBook.searchByContact("38096").size());
        assertEquals(1, phoneBook.searchByContact("a").size());
        assertEquals(1, phoneBook.searchByContact("i").size());
        assertEquals(0, phoneBook.searchByContact("x").size());

    }

    @Test
    public void testDelete() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        phoneBook.delete("akim@gmail.com");

        assertEquals(2, phoneBook.sort().size());

        phoneBook.delete("iiv@gmail.com");

        assertEquals(2, phoneBook.sort().size());

        phoneBook.delete("+380931234567");

        assertEquals(1, phoneBook.sort().size());

        phoneBook.delete("380671112111");

        assertEquals(0, phoneBook.sort().size());

        phoneBook.delete("380671112111");

        assertEquals(0, phoneBook.sort().size());

    }

    @Test
    public void testExists() {

        try {
            init();
        } catch (SQLException exception) {
            throw new AssertionError("Test failed with " + exception.getClass().getSimpleName());
        }

        PhoneBookService phoneBook = getNewPhoneBook();

        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "+380931234567")));
        assertTrue(phoneBook.exists(new Contact(ContactType.PHONE, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "+380681112111")));

        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@ukr.net")));
        assertTrue(phoneBook.exists(new Contact(ContactType.EMAIL, "akim@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "iiv@gmail.com")));

        assertFalse(phoneBook.exists(new Contact(ContactType.EMAIL, "380671112111")));

        assertFalse(phoneBook.exists(new Contact(ContactType.PHONE, "iiv@ukr.net")));

    }

    private static void init() throws SQLException {

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(ApplicationProperties.getProperty("test.db.url"));
        config.setUsername(ApplicationProperties.getProperty("db.username"));
        config.setPassword(ApplicationProperties.getProperty("db.password"));

        config.setMinimumIdle(Integer.parseInt(ApplicationProperties.getProperty("db.minimum_idle")));
        config.setMaximumPoolSize(Integer.parseInt(ApplicationProperties.getProperty("db.maximum_pool_size")));

        JdbcTemplate template = new JdbcTemplate(new HikariDataSource(config));

        template.update("TRUNCATE TABLE person, contact", new Object[]{});

    }

    private static PhoneBookService getNewPhoneBook() {

        PhoneBookService phoneBook = new DBPhoneBook(
                ApplicationProperties.getProperty("test.db.url"),
                ApplicationProperties.getProperty("db.username"),
                ApplicationProperties.getProperty("db.password")
        );

        Person person;
        List<Contact> contacts = new ArrayList<>();

        person = new Person("Иван", "Иванов");

        contacts.add(new Contact(ContactType.PHONE, "+380931234567"));
        contacts.add(new Contact(ContactType.EMAIL, "iiv@ukr.net"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Сергей", "Антоненко");

        contacts.add(new Contact(ContactType.PHONE, "380671112111"));

        person.setContacts(contacts);

        phoneBook.add(person);

        contacts = new ArrayList<>();

        person = new Person("Andrew", "Kim");

        contacts.add(new Contact(ContactType.EMAIL, "akim@gmail.com"));

        person.setContacts(contacts);

        phoneBook.add(person);

        return phoneBook;

    }

}
