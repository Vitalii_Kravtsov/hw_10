package model.service.db;

import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
public class JdbcTemplate {

    private final DataSource dataSource;

    public <T> List<T> query(String query, Object[] params, RowMapper<T> mapper) throws SQLException {

        try(Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }

            ResultSet rs = statement.executeQuery();

            List<T> list = new ArrayList<>(rs.getFetchSize());

            while (rs.next()) {
                list.add(mapper.map(rs));
            }

            return list;

        }

    }

    public <T> T queryForObject(String query, Object[] params, RowMapper<T> mapper) throws SQLException {

        try(Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }

            ResultSet rs = statement.executeQuery();

            if (rs.next())
                return mapper.map(rs);

            return null;

        }

    }

    public int update(String query, Object[] params) throws SQLException {

        try(Connection connection = this.dataSource.getConnection()) {

            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }

            return statement.executeUpdate();

        }

    }

}
