package model.service.network.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;


@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactDto {

    int id;
    String name;
    List<RecordDto> records;

}
