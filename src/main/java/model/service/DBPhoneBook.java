package model.service;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import model.service.db.JdbcTemplate;
import util.ApplicationProperties;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DBPhoneBook implements PhoneBookService {

    private static final int MINIMUM_IDLE = Integer.parseInt(ApplicationProperties.getProperty("db.minimum_idle"));
    private static final int MAXIMUM_POOL_SIZE = Integer.parseInt(ApplicationProperties.getProperty("db.maximum_pool_size"));

    private final JdbcTemplate template;

    public DBPhoneBook(String url, String username, String password) {

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);

        config.setMinimumIdle(MINIMUM_IDLE);
        config.setMaximumPoolSize(MAXIMUM_POOL_SIZE);

        this.template = new JdbcTemplate(new HikariDataSource(config));

    }

    @Override
    public boolean add(Person person) {

        try {

            long id = this.template.queryForObject(
                    "INSERT INTO person (first_name, last_name) VALUES (?, ?) RETURNING id",
                    new Object[]{person.getFirstName(), person.getLastName()},
                    rs -> rs.getLong("id")
            );

            for (Contact c : person.getContacts()) {
                this.template.update(
                        "INSERT INTO contact (person_id, type, value) VALUES (?, ?::contact_type, ?)",
                        new Object[]{id, c.getType().name().toLowerCase(), c.getValue()}
                );
            }

            return true;

        } catch (SQLException | NullPointerException exception) {
            return false;
        }

    }

    @Override
    public List<Person> sort() {

        try {

            List<Person> persons = this.template.query(
                    "SELECT * FROM person ORDER BY last_name, first_name", new Object[]{},
                    rs -> new Person(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
                    );

            for (Person person : persons)
                person.setContacts(this.template.query(
                        "SELECT * FROM contact WHERE person_id = ?", new Object[]{person.getId()},
                        rs -> new Contact(ContactType.valueOf(rs.getString("type").toUpperCase()), rs.getString("value"))
                ));

            return persons;

        } catch (SQLException exception) {
            return null;
        }

    }

    @Override
    public List<Person> filter(ContactType type) {

        try {

            List<Person> persons = this.getPersons();

            List<Person> filtered = new ArrayList<>();

            for (Person person : persons) {

                List<Contact> contacts = new ArrayList<>();

                for (Contact contact : person.getContacts())
                    if (contact.getType().equals(type))
                        contacts.add(contact);

                if (contacts.size() != 0) {

                    Person newPerson = new Person(person.getFirstName(), person.getLastName());

                    newPerson.setContacts(contacts);

                    filtered.add(newPerson);

                }

            }

            return filtered;

        } catch (SQLException exception) {
            return null;
        }

    }

    @Override
    public List<Person> searchByName(String pattern) {

        try {

            List<Person> persons = this.template.query(
                    "SELECT * FROM person WHERE lower(first_name) LIKE ?", new Object[]{"%" + pattern.toLowerCase() + "%"},
                    rs -> new Person(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
            );

            for (Person person : persons)
                person.setContacts(this.template.query(
                        "SELECT * FROM contact WHERE person_id = ?", new Object[]{person.getId()},
                        rs -> new Contact(ContactType.valueOf(rs.getString("type").toUpperCase()), rs.getString("value"))
                ));

            return persons;

        } catch (SQLException exception) {
            return null;
        }

    }

    @Override
    public List<Person> searchByContact(String pattern) {

        try {

            List<Person> persons = this.getPersons();

            List<Person> result = new ArrayList<>();

            for (Person person : persons) {

                for (Contact contact : person.getContacts())
                    if (contact.getValue().startsWith(pattern)) {

                        result.add(person);

                        break;

                    }

            }

            return result;

        } catch (SQLException exception) {
            return null;
        }

    }

    @Override
    public boolean delete(String value) {

        try {

            long personId = this.template.queryForObject(
                    "SELECT * FROM contact WHERE value = ?", new Object[]{value},
                    rs -> rs.getLong("person_id")
            );

            return this.template.update("DELETE FROM person WHERE id = ?", new Object[]{personId}) != 0;

        } catch (SQLException | NullPointerException exception) {
            return false;
        }

    }

    @Override
    public boolean exists(Contact contact) {

        try {

            return this.template.queryForObject(
                    "SELECT * FROM contact WHERE type = ?::contact_type AND value = ?",
                    new Object[]{contact.getType().name().toLowerCase(), contact.getValue()},
                    rs -> new Contact(ContactType.valueOf(rs.getString("type").toUpperCase()), rs.getString("value"))
            ) != null;

        } catch (SQLException exception) {
            return false;
        }

    }

    private List<Person> getPersons() throws SQLException {

        List<Person> persons = this.template.query(
                "SELECT * FROM person", new Object[]{},
                rs -> new Person(rs.getLong("id"), rs.getString("first_name"), rs.getString("last_name"))
        );

        for (Person person : persons)
            person.setContacts(this.template.query(
                    "SELECT * FROM contact WHERE person_id = ?", new Object[]{person.getId()},
                    rs -> new Contact(ContactType.valueOf(rs.getString("type").toUpperCase()), rs.getString("value"))
            ));

        return persons;

    }

}
