package model.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import model.Person;
import model.contact.Contact;
import model.contact.ContactType;
import model.service.network.dto.ContactDto;
import model.service.network.dto.RecordDto;
import model.service.network.dto.Response;
import model.service.network.dto.UserDto;
import model.service.network.mapper.Mapper;
import util.ApplicationProperties;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
public class NetworkPhoneBook implements PhoneBookService {

    private static final String URL_PREFIX = ApplicationProperties.getProperty("network.url.prefix");

    private String token;

    private final HttpClient client;
    private final Mapper<ContactDto, Person> mapper;
    private final ObjectMapper objectMapper;

    private final String login;
    private final String password;

    @Override
    public boolean add(Person person) {

        if (this.token == null) this.token = this.login();

        if (this.token == null) return false;

        try {

            HttpRequest request = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofByteArray(this.objectMapper.writeValueAsBytes(this.mapper.toDto(person))))
                    .uri(URI.create(URL_PREFIX + "/contacts/add"))
                    .header("Authorization", "Bearer " + this.token)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<byte[]> response = this.client.send(request, HttpResponse.BodyHandlers.ofByteArray());

            Response responseDto = this.objectMapper.readValue(response.body(), Response.class);

            return responseDto.isOk();

        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
            return false;
        }

    }

    @Override
    public List<Person> sort() {

        List<Person> persons = this.getPersons();

        return persons == null ? null : persons.stream().sorted().collect(Collectors.toList());

    }

    @Override
    public List<Person> filter(ContactType type) {

        List<Person> persons = this.getPersons();

        if (persons == null)
            return null;

        List<Person> filtered = new ArrayList<>();

        for (Person person : persons) {

            List<Contact> contacts = new ArrayList<>();

            for (Contact contact : person.getContacts())
                if (contact.getType().equals(type))
                    contacts.add(contact);

            if (contacts.size() != 0) {

                Person newPerson = new Person(person.getFirstName(), person.getLastName());

                newPerson.setContacts(contacts);

                filtered.add(newPerson);

            }

        }

        return filtered;

    }

    @Override
    public List<Person> searchByName(String pattern) {

        if (this.token == null) this.token = this.login();

        if (this.token == null) return null;

        try {

            ContactDto dto = new ContactDto();

            dto.setName(pattern);

            HttpRequest request = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofByteArray(this.objectMapper.writeValueAsBytes(dto)))
                    .uri(URI.create(URL_PREFIX + "/contacts/find"))
                    .header("Authorization", "Bearer " + this.token)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            return this.sendPost(request);

        } catch (IOException | InterruptedException exception) {
            return null;
        }

    }

    @Override
    public List<Person> searchByContact(String pattern) {

        if (this.token == null) this.token = this.login();

        if (this.token == null) return null;

        try {

            RecordDto dto = new RecordDto();

            dto.setValue(pattern);

            HttpRequest request = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofByteArray(this.objectMapper.writeValueAsBytes(dto)))
                    .uri(URI.create(URL_PREFIX + "/contacts/find"))
                    .header("Authorization", "Bearer " + this.token)
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            return this.sendPost(request);

        } catch (IOException | InterruptedException exception) {
            return null;
        }

    }

    @Override
    public boolean delete(String value) {
        return false;
    }

    @Override
    public boolean exists(Contact contact) {

        List<Person> persons = this.getPersons();

        if (persons == null)
            return false;

        for (Person person : persons)
            for (Contact c : person.getContacts())
                if (c.equals(contact))
                    return true;

        return false;

    }

    private List<Person> getPersons() {

        if (this.token == null) this.token = this.login();

        if (this.token == null) return null;

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(URL_PREFIX + "/contacts"))
                .header("Authorization", "Bearer " + this.token)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();

        try {
            return this.sendPost(request);
        } catch (IOException | InterruptedException exception) {
            return null;
        }

    }

    private String login() {

        UserDto userDto = new UserDto();

        userDto.setLogin(this.login);
        userDto.setPassword(this.password);

        try {

            HttpRequest request = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofByteArray(this.objectMapper.writeValueAsBytes(userDto)))
                    .uri(URI.create(URL_PREFIX + "/login"))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .build();

            HttpResponse<byte[]> response = this.client.send(request, HttpResponse.BodyHandlers.ofByteArray());

            Response responseDto = this.objectMapper.readValue(response.body(), Response.class);

            if (responseDto.isOk() && responseDto.getToken() != null)
                return responseDto.getToken();

            return null;

        } catch (IOException | InterruptedException exception) {
            return null;
        }

    }

    private List<Person> sendPost(HttpRequest request) throws InterruptedException, IOException {

        HttpResponse<byte[]> response = this.client.send(request, HttpResponse.BodyHandlers.ofByteArray());

        Response responseDto = this.objectMapper.readValue(response.body(), Response.class);

        if (responseDto.isOk() && responseDto.getContacts() != null)
            return this.mapper.toModel(responseDto.getContacts().stream().distinct().collect(Collectors.toList()));

        return null;

    }

}
