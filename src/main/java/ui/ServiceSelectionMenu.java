package ui;

import lombok.RequiredArgsConstructor;
import model.service.PhoneBookService;

import java.util.InputMismatchException;
import java.util.Scanner;


@RequiredArgsConstructor
public class ServiceSelectionMenu {

    private final PhoneBookService[] services;
    private final Scanner scanner;

    private void display() {

        System.out.println("Выберите реализацию телефонной книги:\n");

        for (int i = 0; i < this.services.length; i++)
            System.out.printf("\t%s. %s\n", i + 1, this.services[i].getClass().getSimpleName());

        System.out.println();

    }

    private int choose() {

        System.out.print("\nВведите номер реализации: ");

        int service_number = scanner.nextInt();

        scanner.nextLine();

        return service_number;

    }

    private boolean validate(int service_number) {
        return service_number > 0 && service_number <= this.services.length;
    }

    public PhoneBookService run() {

        this.display();

        int service_number = 0;
        boolean valid = false;

        while (!valid) {
            try {
                service_number = this.choose();
                valid = this.validate(service_number);
                if (!valid)
                    System.out.println("Неправильно введен номер реализации. Введите еще раз.");
            } catch (InputMismatchException exception) {
                System.out.println("Неправильно введен номер реализации. Введите еще раз.");
                this.scanner.nextLine();
            }
        }

        return this.services[service_number-1];

    }

}
