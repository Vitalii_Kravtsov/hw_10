DROP TABLE IF EXISTS contact;
DROP TABLE IF EXISTS person;

DROP TYPE IF EXISTS contact_type;

CREATE TYPE contact_type AS ENUM ('phone', 'email');

CREATE TABLE IF NOT EXISTS person (
	id SERIAL8 NOT NULL PRIMARY KEY,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS contact (
	id SERIAL8 NOT NULL PRIMARY KEY,
	person_id INT8 NOT NULL,
	type contact_type NOT NULL,
	value VARCHAR(255) NOT NULL,
	FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE
)
